#!bash
# Compile split pdfs into pdf

mkdir temp/
for i in {4..30}
do
  n=`printf %02d $i`
  convert split/${n}* temp/$n.pdf
done

pdfunite temp/* output.pdf

rm -r temp/
