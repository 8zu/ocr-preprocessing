from glob import glob
from PIL import Image

images = glob('*.jpg')

for path in images:
  name = path.split('.')[0]
  img = Image.open(path)
  w, h = img.size
  upper = img.crop((0, 0, w, h // 2))
  lower = img.crop((0, h // 2, w, h))
  upper.save('split/' + name + 'a.jpg')
  lower.save('split/' + name + 'b.jpg')
