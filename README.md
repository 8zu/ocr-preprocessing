1. Split the images into half, so that they can be OCR'ed correctly (most OCR softare assumes what it sees is a consecutive piece of text)
   ```
   pipenv run python split_images.py
   ```
2. Convert the images into one PDF
   ```
   bash compile_pdf.sh
   ```
3. I used this OCR site https://www.onlineocr.net
